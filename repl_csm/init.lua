minetest.register_chatcommand("e", {
	params = "<code>",
	description = "Evaluate <code>, print the results",
	func = function(param)
		if not param then
			return false
		end
		local ok, fn = pcall(function()
			return loadstring(param)
		end)
		if ok ~= true then
			return false, "[err loading]: "..fn
		end
		local ok, res = pcall(fn)
		if ok == true then
			if res then
				return true, "[ok]: "..dump2(res)
			else
				return true, "[ok]"
			end
		else
			return false, "[err executing]: "..res
		end
	end,
})
