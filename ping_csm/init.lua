local dndl=nil
local dnd=false
local mcount=0

local function get_dnd_hud_text() return " mention DND (" .. mcount .. ")" end

local s=Settings and Settings("ping.conf") or nil -- apparently this isn't added yet?

local snd=s and s.get("sound") or "awards_got_generic"
local vol=s and tonumber(s.get("volume")) or 1.0
local col=s and s.get("color") or "#FF1111"

local hist={}

local function log_history(msg)
	-- escape string for later
	local n=string.gsub(msg, "([%^%$%(%)%%%.%[%]%*%+%-%?])", "%%%1")
	table.insert(hist, n)
	return false
end

local function do_ping(msg)
	for k, v in ipairs(hist) do
		if v ~= "" and string.match(msg, v) then
			table.remove(hist, k)
			return false
		end
	end
	local n=minetest.localplayer:get_name()
	if string.match(msg, "%W"..n.."%W") then
		minetest.display_chat_message(string.gsub(msg, "(%W)("..n..")(%W)", "%1"..minetest.colorize(col, "%2").."%3"))
		if not dnd then
			minetest.sound_play(snd, { gain=vol } )
		else
			mcount=mcount+1
			minetest.localplayer:hud_change(
				dndl,
				"text",
				get_dnd_hud_text()
			)
		end
		return true
	end
end

minetest.register_chatcommand("dnd", {
	description = "Toggle *ping Do-Not-Disturb",
	func = function(param)
		dnd=not dnd
		local s="off"
		if dnd then
			s="on"
			dndl=minetest.localplayer:hud_add({
				position={x=0,y=0.5},
				number=0xFF1111,
				text=" mention DND",
				alignment={x=1,y=0},
			})
		elseif dndl ~= nil then
			minetest.localplayer:hud_remove(dndl)
			dndl=nil
			mcount=0
		end
		return true, "DND is "..s
	end,
})

minetest.register_on_sending_chat_message(log_history)
minetest.register_on_receiving_chat_message(do_ping)
